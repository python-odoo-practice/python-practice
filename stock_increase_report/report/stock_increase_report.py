from openerp import models, api
from datetime import datetime


class StockIncreaseReport(models.AbstractModel):
    _name = 'report.stock_increase_report.report_stock_increase'

    def get_pickings(self, docs):
        pickings = []

        if docs:
            q_date=docs.from_date

            self._cr.execute(
                "select stock_move.id,stock_move.state,stock_move.date,stock_move.product_id,stock_move.product_uom_qty,stock_move.product_uos_qty,(select default_code from product_product where product_product.id=stock_move.product_id) as sku, (select product_name from product_product where product_product.id=stock_move.product_id) as product_name, (select sum(qty) from stock_quant,stock_quant_move_rel where stock_quant.id=stock_quant_move_rel.quant_id and stock_quant_move_rel.move_id=stock_move.id) as t_qty from stock_move where stock_move.date >%s and stock_move.product_uom_qty < (select sum(qty) from stock_quant,stock_quant_move_rel where stock_quant.id=stock_quant_move_rel.quant_id and stock_quant_move_rel.move_id=stock_move.id)",
                (q_date,))
            all_out_product_qty = self._cr.dictfetchall()

            pickings= all_out_product_qty





                # import pdb
                # pdb.set_trace()

        return pickings

    @api.model
    def render_html(self, docids, data=None):

        self.model = self.env.context.get('active_model')
        docs = self.env[self.model].browse(self.env.context.get('active_id'))
        pickings = self.get_pickings(docs)
        docargs = {
            'doc_ids': self.ids,
            'doc_model': self.model,
            'docs': docs,
            'pickings': pickings,
        }
        return self.env['report'].render('stock_increase_report.report_stock_increase', docargs)
