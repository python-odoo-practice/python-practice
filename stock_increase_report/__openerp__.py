{
    'name': "Stock Increase Report",
    'version': '8.0.1.0.0',
    'summary': """Stock Increase Report With Filter""",
    'description': """ With this module, we can perform stock increase report with optional filters such as daterange, warehouse etc.""",
    'author': "Ashif Raihun",
    'website': "https://www.sindabad.com",
    'company': '',
    'maintainer': '',
    'category': 'Stock',
    'depends': ['sale', 'order_approval_process'],
    'data': [
            'wizard/stock_increase_report_filter.xml',
            'report/stock_increase_report.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': False,
}